<div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v9.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/vi_VN/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your Chat Plugin code -->
      <div class="fb-customerchat"
        attribution=setup_tool
        page_id="107560407792404"
  theme_color="#4e7362"
  logged_in_greeting="Xin chào, cảm ơn bạn đã quan tâm đến Yu Cosmetics. Bạn cần tư vấn sản phẩm nào?"
  logged_out_greeting="Xin chào, cảm ơn bạn đã quan tâm đến Yu Cosmetics. Bạn cần tư vấn sản phẩm nào?">
      </div>
<script>
jQuery(document).ready(function() {

    let keyFirst = jQuery("#list_stockist_locator li").first().attr("data-key");
    let codeFirst = jQuery("#input_location_map_" + keyFirst).val();
    jQuery("#box_map_stockist_locator").html(codeFirst);
    jQuery("#box_map_stockist_locator iframe").css({
        'width': '100%',
        'height': '500px'
    });

    jQuery("#filter_locator").on("keyup", function() {
        var value = jQuery(this).val().toLowerCase();
        jQuery("#list_stockist_locator li").filter(function() {
            let rs = jQuery(this).text().toLowerCase().indexOf(value);
            jQuery(this).toggle(rs > -1);
        });
    });

    jQuery(".location-list-container").on("click", function() {
        jQuery('.box-loading-sub, .box-loading-sub .loader-sub').show();
        setTimeout(() => {
            jQuery('.box-loading-sub, .box-loading-sub .loader-sub').hide();
        }, 1500);
        var key = jQuery(this).attr("data-key");
        var iframeMap = jQuery("#input_location_map_" + key).val();
        jQuery("#box_map_stockist_locator").html(iframeMap);
        jQuery("#box_map_stockist_locator iframe").css({
            'width': '100%',
            'height': '500px'
        });

    });

    jQuery('.btn-register-partner').on('click', function() {
        jQuery('.form-register-partner').slideToggle('slow');
    });

    // woocommerce-tabs
    jQuery('.woocommerce-tabs .woocommerce-Tabs-panel').removeClass('active');
    jQuery('.woocommerce-tabs #tab-reviews').addClass('active');

    // products hover
    jQuery('.row .product-small').removeClass('has-hover');

});
</script>

<script>
// scroll product tags description
jQuery(function($) {

    if ($(window).width() > 850) {
        $(window).bind('scroll', function() {
            var currentTop = $(window).scrollTop();
            var elems = $('.scrollspy');
            elems.each(function(index) {
                var elemTop = $(this).offset().top;
                //var elemBottom = elemTop + $(this).height() + 50;
                if (currentTop >= elemTop) {
                    jQuery('.shop-container .product-tab-fixed').addClass('scroll-fixed');
                } else {
                    jQuery('.shop-container .product-tab-fixed').removeClass('scroll-fixed');
                }
            })
        });
    }

    jQuery('.product-tab-description .tablinks').click(function() {

        jQuery('.product-tab-description .tablinks').removeClass('scrollActive');
        jQuery('#tab-title-reviews').removeClass('scrollActive');

        jQuery(this).addClass('scrollActive');
        let id = jQuery(this).attr('data-id');
        jQuery(id + ' .tablinks').addClass('scrollActive');
        if (id == '#tab-title-reviews') {
            jQuery(id).addClass('scrollActive');
        }

        jQuery('html, body').animate({
            scrollTop: jQuery(id).offset().top - 80
        }, 700);
    });

});
</script>