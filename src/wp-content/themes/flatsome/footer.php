<?php
/**
 * The template for displaying the footer.
 *
 * @package flatsome
 */

global $flatsome_opt;
?>

</main>

<footer id="footer" class="footer-wrapper">

	<?php do_action('flatsome_footer'); ?>
	<?php get_template_part('template-parts/footer/footer-script'); ?>
</footer>

</div>

<?php wp_footer(); ?>

</body>
</html>
